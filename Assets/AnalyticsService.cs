﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameAnalyticsSDK;

public class AnalyticsService : MonoBehaviour
{
    private AnalyticsService _analyticsService = null;
    public AnalyticsService analyticsService 
    {
        get
        {
            if (_analyticsService == null)
                _analyticsService = this;
            else
                Destroy(this.gameObject);

            DontDestroyOnLoad(this.gameObject);
            return _analyticsService;
        }
    }

    private void Awake()
    {
        GameAnalytics.Initialize();
    }
}
